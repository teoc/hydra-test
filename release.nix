{pulls}:
builtins.trace pulls {
  boo = (import <nixpkgs> {}).hello;
  bar = (import <nixpkgs> {}).git;
  blem = (import <nixpkgs> {}).coreutils;
}
